
import logging
import os
import subprocess
import unittest
import sys
import z3c.rml.tests

from z3c.rml import rml2pdf, attr

from mako.template import Template


mytemplate = Template(filename='btest.xml')

figures=[
#(number,name,x-size,y-size,)

]

f=open("btest.rml","w")
f.write(mytemplate.render(figures=figures))
f.close()

rml2pdf.go("btest.rml", "test.pdf")

